//
//  CDAValueFormater.swift
//  Charts-Notio-test
//
//  Created by Pascale Beaulac on 2020-01-30.
//  Copyright © 2020 Notio Technologies inc. All rights reserved.
//

import Foundation
import Charts

public class CDAValueFormater: NSObject, IAxisValueFormatter {
    private let dateFormatter = DateFormatter()
    
    override init() {
        super.init()
        
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return "\(Measurement(value: value, unit: UnitDuration.seconds))"
    }
}
