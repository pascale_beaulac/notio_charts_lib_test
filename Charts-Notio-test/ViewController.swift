//
//  ViewController.swift
//  Charts-Notio-test
//
//  Created by Pascale Beaulac on 2020-01-30.
//  Copyright © 2020 Notio Technologies inc. All rights reserved.
//

import UIKit
import Charts

class ViewController: UIViewController, ChartViewDelegate {
    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet weak var minValLabel: UILabel!
    @IBOutlet weak var maxValLabel: UILabel!
    
    var notioData: [NotioData]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let path = Bundle.main.path(forResource: "notiodata", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                notioData = try JSONDecoder().decode([NotioData].self, from: data)
            }catch {
                fatalError()
            }
        }
        
        chartView.chartDescription?.enabled = true
        chartView.chartDescription?.text = "CdA and Power"
        chartView.delegate = self
        chartView.dragEnabled = true
        chartView.setScaleEnabled(true)
        chartView.pinchZoomEnabled = false
        chartView.highlightPerDragEnabled = true
        chartView.autoScaleMinMaxEnabled = false
        chartView.backgroundColor = .systemTeal
        chartView.scaleYEnabled = false
        chartView.legend.enabled = true
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10, weight: .light)
        xAxis.labelTextColor = UIColor.black
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = false
        xAxis.centerAxisLabelsEnabled = true
//        xAxis.granularity = 5
        xAxis.valueFormatter = CDAValueFormater()
        
        self.setDataCount(300, range: 30)
        
        let leftAxis = chartView.leftAxis
        leftAxis.labelPosition = .outsideChart
        leftAxis.labelFont = .systemFont(ofSize: 10, weight: .light)
        leftAxis.drawGridLinesEnabled = true
        leftAxis.granularityEnabled = true
        leftAxis.granularity = 0.001
        //        leftAxis.axisMinimum = chartView.chartYMin - 10
        //        leftAxis.axisMaximum = chartView.chartYMax + 10
        leftAxis.yOffset = -9
        leftAxis.labelTextColor = UIColor.black
        leftAxis.labelPosition = .outsideChart
        
        let rightAxis = chartView.rightAxis
        rightAxis.labelPosition = .insideChart
        rightAxis.labelFont = .systemFont(ofSize: 10, weight: .light)
        rightAxis.drawGridLinesEnabled = true
        rightAxis.granularityEnabled = true
        rightAxis.granularity = 0.001
        //        rightAxis.axisMinimum = chartView.chartYMin - 10
        //        rightAxis.axisMaximum = chartView.chartYMax + 10
//        rightAxis.yOffset = 9
        rightAxis.labelTextColor = UIColor.black
        rightAxis.labelPosition = .insideChart
        
        
        chartView.rightAxis.enabled = false
        
        let marker = BalloonMarker(color: UIColor(white: 180/255, alpha: 1),
                                   font: .systemFont(ofSize: 12),
                                   textColor: .white,
                                   insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        marker.valueFormat = "%.3f"
        chartView.marker = marker
        
        chartView.legend.form = .line
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        chartViewDidEndPanning(self.chartView)
    }
    
    func setDataCount(_ count: Int, range: UInt32) {
        
        let values1 = notioData!.map { (data) -> ChartDataEntry in
            return ChartDataEntry(x:Double(data.timestamp), y:data.cdA)
        }
        
        let set1 = LineChartDataSet(entries: values1, label: "CdA")
        set1.axisDependency = .right
        set1.setColor(UIColor.black)
        set1.lineWidth = 2
        set1.drawCirclesEnabled = false
        set1.drawIconsEnabled = false
        set1.drawValuesEnabled = false
        set1.fillAlpha = 0.26
        set1.fillColor = UIColor.blue
        set1.highlightColor = UIColor.red
        set1.drawCircleHoleEnabled = false
        set1.mode = .horizontalBezier
        set1.valueFont = .systemFont(ofSize: 9)
        set1.formLineWidth = 1
        set1.formSize = 15
        
        //        let gradientColors = [ChartColorTemplates.colorFromString("#00ff0000").cgColor,
        //                              ChartColorTemplates.colorFromString("#ffff0000").cgColor]
        //        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        //
        //        set1.fillAlpha = 1
        //        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        //        set1.drawFilledEnabled = true
        
        let values2 = notioData!.map { (data) -> ChartDataEntry in
            return ChartDataEntry(x:Double(data.timestamp), y:data.speed)
        }
        
        let set2 = LineChartDataSet(entries: values2, label: "Speed")
        set2.axisDependency = .left
        set2.setColor(UIColor.green)
        set2.lineWidth = 2
        set2.drawCirclesEnabled = false
        set2.drawIconsEnabled = false
        set2.drawValuesEnabled = false
        set2.fillAlpha = 0.26
        set2.fillColor = UIColor.yellow
        set2.highlightColor = UIColor.purple
        set2.drawCircleHoleEnabled = false
        set2.mode = .linear
        set2.valueFont = .systemFont(ofSize: 9)
        set2.formLineWidth = 1
        set2.formSize = 15
        
        //        let gradientColors2 = [ChartColorTemplates.colorFromString("#0000ff00").cgColor,
        //                              ChartColorTemplates.colorFromString("#ff0000ff").cgColor]
        //        let gradient2 = CGGradient(colorsSpace: nil, colors: gradientColors2 as CFArray, locations: nil)!
        //
        //        set2.fillAlpha = 1
        //        set2.fill = Fill(linearGradient: gradient2, angle: 90) //.linearGradient(gradient, angle: 90)
        //        set2.drawFilledEnabled = true
        
        let values3 = notioData!.map { (data) -> ChartDataEntry in
            return ChartDataEntry(x:Double(data.timestamp), y:data.power)
        }
        
        let set3 = LineChartDataSet(entries: values3, label: "Power")
        set3.axisDependency = .left
        set3.setColor(UIColor.red)
        set3.lineWidth = 2
        set3.drawCirclesEnabled = false
        set3.drawIconsEnabled = false
        set3.drawValuesEnabled = false
        set3.fillAlpha = 0.26
        set3.fillColor = UIColor.yellow
        set3.highlightColor = UIColor.purple
        set3.drawCircleHoleEnabled = false
        set3.mode = .linear
        set3.valueFont = .systemFont(ofSize: 9)
        set3.formLineWidth = 1
        set3.formSize = 15
        
        let data = LineChartData(dataSets: [set1, set2, set3])
        //        data.setValueTextColor(.blue)
        //        data.setValueFont(.systemFont(ofSize: 9, weight: .light))
        
        chartView.data = data
        
    }
    
    func chartScaled(_ chartView: ChartViewBase, scaleX: CGFloat, scaleY: CGFloat) {
        guard  let cv = chartView as? LineChartView else {
            return
        }
        self.minValLabel.text = "Min value is " + String(format: "%.3f", cv.lowestVisibleX)
        self.maxValLabel.text  = "Max value is " + String(format: "%.3f", cv.highestVisibleX)
        print("Visible : \(cv.visibleXRange), \(cv.lowestVisibleX), \(cv.highestVisibleX)")
    }
    
    func chartViewDidEndPanning(_ chartView: ChartViewBase) {
        guard  let cv = chartView as? LineChartView else {
            return
        }
        self.minValLabel.text = "Min value is " + String(format: "%.3f", cv.lowestVisibleX)
        self.maxValLabel.text  = "Max value is " + String(format: "%.3f", cv.highestVisibleX)
        print("Visible : \(cv.visibleXRange), \(cv.lowestVisibleX), \(cv.highestVisibleX)")
    }
}

